package com.smarschools.cloud.authserver.services;


import com.smarschools.cloud.authserver.domain.User;
import com.smarschools.cloud.authserver.vo.UserRegistrationForm;

import java.util.Optional;

public interface UserService {

    Optional<User> findByEmail(String email);

    User save(UserRegistrationForm registration);

    User updatePassword(String updatedPassword,User user);

}