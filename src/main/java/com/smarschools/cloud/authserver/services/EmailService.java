package com.smarschools.cloud.authserver.services;

import com.smarschools.cloud.authserver.vo.Mail;

public interface EmailService {

    void sendEmail(Mail mail);

}
