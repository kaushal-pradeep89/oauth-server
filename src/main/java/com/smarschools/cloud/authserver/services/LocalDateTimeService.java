package com.smarschools.cloud.authserver.services;

import java.time.LocalDateTime;

public interface LocalDateTimeService {

    LocalDateTime current();

}
