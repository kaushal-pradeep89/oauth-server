package com.smarschools.cloud.authserver.services.impl;

import com.smarschools.cloud.authserver.services.LocalDateTimeService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LocalDateTimeServiceImpl implements LocalDateTimeService {

    @Override
    public LocalDateTime current() {
        return LocalDateTime.now();
    }

}
