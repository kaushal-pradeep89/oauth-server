package com.smarschools.cloud.authserver.controllers;


import com.smarschools.cloud.authserver.domain.User;
import com.smarschools.cloud.authserver.repository.UserRepository;
import com.smarschools.cloud.authserver.vo.RegisterUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @PostMapping("/user")
    public void createUser(RegisterUser user){
        userRepository.save(new User());
    }

}
